### Taxonomic affiliation

These results have been generated after the [CSS]\ [@css] normalization method.

The Amplicon-Seq pipeline provides taxonomic affiliation of your data at different level (Kingdom, Phylum, Class, Order, Family, Genus, Species).

### Beta diversity

$description_metric$

$link_metric1$ 

$link_metric2$ 


