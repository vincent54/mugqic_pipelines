### Taxonomic affiliation

These results have been generated after a rarefaction step. All the samples have been rarefied to **$single_rarefaction_depth$** sequences.

The Amplicon-Seq pipeline provides taxonomic affiliation of your data at different level (Kingdom, Phylum, Class, Order, Family, Genus, Species).

### Beta diversity

$description_metric$

$link_metric1$ 

$link_metric2$ 

