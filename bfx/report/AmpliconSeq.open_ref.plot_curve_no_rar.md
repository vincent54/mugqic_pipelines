### Rarefaction curve for each sample

A rarefaction curve for each sample is plotted (observed OTUs metric) in order to estimate if the sequencing depth is enough to get all the species in the sample. It can also help to choose the rarefaction threshold for all samples. 

<img src="fig/open_ref_alpha_diversity/alpha.rarefaction_sample.png" width="100%" />
<p>
Rarefaction curve for each sample ([download high resolution figure](fig/open_ref_alpha_diversity/alpha.rarefaction_sample.png))


