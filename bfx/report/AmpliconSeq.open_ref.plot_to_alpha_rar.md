### Taxonomic affiliation

These results have been generated after a rarefaction step. All the samples have been rarefied to **$single_rarefaction_depth$** sequences.

The Amplicon-Seq pipeline provides taxonomic affiliation of your data at different level (Kingdom, Phylum, Class, Order, Family, Genus, Species).

#### Bar plot format 

[Interactive html plots available here](fig/open_ref_alpha_diversity/rarefaction/taxonomic_affiliation/bar_charts.html)

#### Krona chart format 

The [Krona]\ [@krona] chart is an interactive pie chart ([available here](fig/open_ref_alpha_diversity/rarefaction/krona_chart/krona_chart.html)).

### Heatmap

The figure below represents a heatmap of the taxonomic distribution (level: Phylum). It provides informations about taxonomic affiliation and relation between samples.

Samples are clustered (x-axis) using euclidean distances. 

![Phylum composition ([download OTU table](fig/open_ref_beta_diversity/rarefaction/heatmap/otumat.tsv) - [download taxon table](fig/open_ref_beta_diversity/rarefaction/heatmap/taxmat.tsv))](fig/open_ref_beta_diversity/rarefaction/heatmap/otu_heatmap.png)

### Alpha diversity 

Alpha diversity is a measure of diversity within a sample. It gives an indication of richness and/or evenness of species present in a sample. The Amplicon-Seq pipeline provides Shannon index, chao1 and observed species metrics.

[Interactive html plots for alpha diversity available here](fig/open_ref_alpha_diversity/rarefaction/alpha_rarefaction/rarefaction_plots.html)

 

